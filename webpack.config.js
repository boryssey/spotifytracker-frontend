const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.join(__dirname, '/dist'),
        filename: 'index_bundle.js',
        publicPath:'/'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
              test: /\.(png|jp(e*)g|svg)$/,
              use: [{
                loader: 'url-loader',
                options: {
                  limit: 8000, // Convert images < 8kb to base64 strings
                  name: 'images/[hash]-[name].[ext]'
                }
              }]
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            favicon: "./src/images/favicon-32x32.png",
            favicon2: "./src/images/favicon-16x16.png",
            template: './src/index.html'
        })
    ]
}
