import React from 'react';
import axios from './index';


class PlaybackApi {
    static getPlayback(username){
        return axios.get(`/users/${username}`);
    }
}

export default PlaybackApi;
