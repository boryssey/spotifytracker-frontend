import React from 'react';
import axios from './index';


class UserInfoApi {
    static getUserInfo(id) {
        return axios.get(`/me/${id}`);
    }
    static changeUsername(id, username){
      return axios.put('/me', {userId: id, display_name: username});
    }
}

export default UserInfoApi;
