import {LOGIN_USER, LOGOUT_USER} from '../actions/types';

const initialState = {
  user:{},
  authenticated: false,
}

export default function(state = initialState, action) {
  switch(action.type) {
    case LOGOUT_USER:
      return {
        ...state,
        user: {},
        authenticated: false,
      }
    case LOGIN_USER:
      return {
        ...state,
        user: action.payload,
        authenticated: true,
      }
    default:
      return state;
  }
}
