import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/IconButton';
import ExitIcon from '@material-ui/icons/ExitToApp';
import logo from '../images/Spotify_logo_without_text.svg'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { Link as RouterLink } from 'react-router-dom';
import Link from '@material-ui/core/Link';
import {connect} from 'react-redux'
import {logoutUser, loginUser} from '../actions/loginActions'
import { withCookies, Cookies } from 'react-cookie';

const styles = theme => ({
  Logo:{
    display: 'flex',
    boxSizing: 'border-box',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop:'auto',
    marginBottom:'auto',
    padding:'5px'
  },
  root: {
    gridArea:'header',
    flexGrow: '1',
    background: '#222326',
    borderBottomStyle: 'solid',
    borderBottomWidth: '0.5px',
    margin:'0',
    // boxShadow: 'none',
  },
  h5:{
    color: 'white',
    flexGrow:1
  },
  user:{
    color:'white',
    fontWeight:900,
    display:'flex',
    alignItems:'center'
  },
  exitButton:{
    color:'white',
    marginTop:'auto',
    padding:'10px'
  }
});



function handleLogin(){
  alert('Hello world');
}

class Header extends React.Component {
  constructor(props) {
    super(props);
    const { cookies } = this.props;

    var authenticated = cookies.get('authenticated') || 'false'
    console.log('hello1')
    if(authenticated === 'false' && this.props.authenticated === true){
      props.logoutUser();
    }
    if(authenticated === 'true' && this.props.authenticated === false){
        console.log('hello')
        let username = cookies.get('username');
        let userId = cookies.get('userId');
        props.loginUser({username, userId});
    }

  }
  loginLink = 'http://localhost:8888/login'
  onLogout = () => {
    this.props.cookies.set('authenticated', 'false', {path:'/', maxAge:3600});
    this.props.logoutUser();
  }


  render(){
    return (
        <AppBar position="static" color="default" className={this.props.classes.root}>
          <Toolbar style={{marginTop:'auto', marginBottom:'auto'}}>
            <Link href="#/">
              <div className={this.props.classes.Logo}>
                <img src={logo} style={{height:'45px', width:'45px', margin:'5px'}}/>
                <Typography variant="h5" color="inherit" className={this.props.classes.h5}>
                  Spotify Tracker
                </Typography>
              </div>
            </Link>
            <div style={{flexGrow:1}}></div>
            {this.props.authenticated
              ? <div className={this.props.classes.user}><Link variant='h6' color='secondary' href='#/me'  >{this.props.user.username}</Link> <Button onClick={this.onLogout} className={this.props.classes.exitButton}><ExitIcon/></Button></div>
              : <Link variant="h6" className={this.props.classes.user} href={this.loginLink}>Login with Spotify</Link>}

          </Toolbar>
        </AppBar>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  authenticated: state.login.authenticated,
  user:state.login.user
})

export default withCookies(connect(mapStateToProps, {logoutUser, loginUser})(withStyles(styles)(Header)));
