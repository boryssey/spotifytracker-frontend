import React from 'react'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import SpotifyPage from '../images/SpotifyPageCropped.png';
import LoginImage from '../images/Login_with_arrow.png';


const styles = theme => ({
  container:{
    display: 'grid',
    background: '#222326;'

  },
  grid:{
    margin:'50px auto auto auto',
    maxWidth:'700px'
  },
  image:{
    width:'100%',
    height:'auto',
    margin:'5px',
    padding:'1px',

  }
});



class Test extends React.Component {
  render () {
    return (
      <div className={this.props.classes.container}>
        <Grid container spacing={8} className={this.props.classes.grid}>
          <Grid item xs={12}>
            <Typography variant="h3" color='secondary'>About</Typography>
            <Typography variant="body1" color='secondary'>Always wanted your friends to be able to know what you are listening to right now?</Typography>
            <Typography variant="body1" color='secondary'>Popular streamer and tired of people asking what song is playing right now?</Typography>
            <Typography variant="body1" color='secondary'>Login using Spotify and get a link where your currently playing track will be always shown.</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="h3" color='secondary'>How to</Typography>
            <Typography variant="body1" color='secondary'>Press the login button on the top right of the page.</Typography>
            <img src={LoginImage} className={this.props.classes.image}/>
            <Typography variant="body1" color='secondary'>You will be redirect to Spotify login page. Here are the permissions Spotify Tracker will ask for:</Typography>
            <img src={SpotifyPage} className={this.props.classes.image}/>
          </Grid>

        </Grid>

      </div>
    )
  }
}

export default withStyles(styles)(Test);
