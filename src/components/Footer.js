import React from 'react'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  text:{
    color:'#919496',
  },
  footer:{
    background:'#222326',
    height:'auto',
    gridArea:'footer',
    textAlign: 'center',
  }
})

function Footer(props){
  return(
    <footer className={props.classes.footer}>
      <Typography variant="caption" className={props.classes.text}>if you have any questions or you found a bug, please contact me by mail: 'boryssey.dev@gmail.com'</Typography>
    </footer>
  )
}


export default withStyles(styles)(Footer);
