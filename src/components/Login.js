import React from 'react'
import {connect} from 'react-redux';
import {loginUser} from '../actions/loginActions'
import { useCookies } from 'react-cookie';


function Login(props) {
    const [cookies, setCookie] = useCookies(['authenticated', 'username', 'userId'])
    if((new URLSearchParams(props.location.search).get('error')) === null){
      let user = {
        username: new URLSearchParams(props.location.search).get('username'),
        userId: new URLSearchParams(props.location.search).get('userid')
      }
      props.loginUser(user)
      setCookie('username', user.username, {path:'/', maxAge:3600});
      setCookie('authenticated', 'true', {path:'/', maxAge:3600});
      setCookie('userId', user.userId, {path:'/', maxAge:3600});
      props.history.push('/me')
    } else{
      props.history.push('/users/boryssey')
    }
    return(
      <div/>
    )
}

export default connect(null, {loginUser})(Login);
