import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux';
import { useCookies } from 'react-cookie';
import {loginUser, logoutUser} from '../actions/loginActions'

import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter
} from "react-router-dom";

function PrivateRoute({ component: Component, ...rest }) {

  return (
    <Route
      {...rest}

      render={props =>
        rest.authenticated ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/",
            }}
          />
        )
      }
    />
  );
}

const mapStateToProps = state => ({
  authenticated: state.login.authenticated,
  user:state.login.user
})


export default connect(mapStateToProps, {loginUser, logoutUser})(PrivateRoute);
