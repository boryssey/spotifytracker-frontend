import React from 'react';
import image404 from '../images/FeelsBadMan.png'
import Typography from '@material-ui/core/Typography';



const styles = {
  text:{
    margin:'auto',
    marginTop:'100px',
    color:'white',
  },
  image:{
    margin:'auto',
    marginTop:'50px',
    width:'494px',
    height:'494px',
  },
}

function NotFound(){
  {document.title =  'Spotify Tracker - Page Not Found'}
  return(
    <div className = 'content' style={{display:"grid", margin:"auto"}}>
      <img src={image404} style={styles.image}/>
      <Typography variant="h1" style={styles.text}>404 Page not found</Typography>
    </div>
  )
}

export default NotFound;
