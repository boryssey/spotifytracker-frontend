import React from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import {withStyles, makeStyles} from '@material-ui/core/styles';
import { CircleLoader as Loader
 } from 'react-spinners';
 import {connect} from 'react-redux';
import PlaybackApi from '../api/PlaybackApi'


import AlbumIcon from '@material-ui/icons/Album';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';


import logo from '../images/Spotify_logo_without_text.svg'
import SvgIcon from '@material-ui/core/SvgIcon';



const theme = createMuiTheme({
  typography:{
    fontFamily:"'Montserrat', sans-serif",
    useNextVariants: true,
    h6:{
      fontSize:'15px',
      fontWeight:'900',
    }
  },
  palette: {
    primary: {
      main: '#222326',
    },
    secondary: {
      light: '#FFFFFF',
      main: '#FFFFFF',
      contrastText: '#ffcc00',
    },
  },
});


const styles = theme => ({
  albumImage:{
    height:'auto',
    width: '25vw',
    minWidth: '250px',
    color:'white'
  },
  content:{
    display: 'grid',
    background:'#222326'
  },
  currentlyPlaying:{
    margin:'1vw auto',
    display:'grid',
    justifyItems:'center',
    maxWidth:'500px',
    padding:'15px'

  },
  playbutton:{
    borderColor:'#FFFFFF',
    borderStyle: 'solid',
    borderWidth: '1px',
    boxShadow: 'none',
    borderRadius: '10px',
    width:'100%',
    height:'70px',
    marginTop: '0.5vw',
    fontWeight:'900'
  },
  logo:{
    height:'40px',
    width:'40px',
    margin:'10px'
  }
})

const PlayBack = withStyles(styles)(props => {
  console.log(props)
  return(
      <div className='currently-playing' className={props.classes.currentlyPlaying}>
        {props.song.image === "" ? <AlbumIcon className={props.classes.albumImage}/> : <img src = {props.song.image} className={props.classes.albumImage} placeholder='Album Image' />}
        <div style={{justifySelf:'left',}}>
        <Typography variant="h6" color='secondary' style={{marginTop:'1vw'}}>{props.song.username} IS LISTENING TO:</Typography>
        <Typography variant="h6" color='secondary' style={{marginTop:'0.5vw'}}>{props.song.artist}</Typography>
        <Typography variant="h6" color='secondary' style={{marginTop: '-2px'}}>{props.song.songname}</Typography>
        </div>
        {props.song.link === "" ? null : <Button variant="contained" href={props.song.link} disableTouchRipple={true} color="primary" className={props.classes.playbutton}>
        <img src={logo} className={props.classes.logo}/>
        Play on spotify
        </Button>}
      </div>
    )
})

class UserPlayback extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      songObj: {
        username:"user",
        artist:'null',
        songname:'not',
        link:'link',
        image:'',
      },
      loading: true,
      usernotfound: false,
      notplaying: false,
    }

    this.user = this.props.match.params.user;
    document.title =  'Spotify Tracker - ' +  this.state.songObj.username + "'s playback";
    this.fetchData = this.fetchData.bind(this);
  }


  fetchData(){
    PlaybackApi.getPlayback(this.user).then((response) => {
      console.log(response)
        if(response.status === 200){
          this.setState({
            songObj: response.data,
            loading: false
          })
        }
        if(response.status === 204){
          this.setState({
            notplaying:true,
            loading:false
          })
          throw new Error(response.statusText)
        }
    }).catch((e)=>{
      if(e.response.status === 404){
        this.setState({
          usernotfound: true,
          loading:false
        })
      }
      console.log(e)
    })
  }

  componentDidMount(){
    this.fetchData()
  }


  render() {
    {document.title =  'Spotify Tracker - ' +  this.state.songObj.username + "'s playback"}
    return (

      <div className='playback-content' className={this.props.classes.content}>

          <MuiThemeProvider theme={theme}>
            {this.state.usernotfound === true ?
              <Typography variant="h6" color='secondary' style={{marginTop:'40px', justifySelf:'center'}}>User not found</Typography>
              : null
            }
            {this.state.notplaying === true ?
              <Typography variant="h6" color='secondary' style={{marginTop:'40px', justifySelf:'center'}}>Nothing is playing at the moment</Typography>
              : null
            }
            {!this.state.notplaying && !this.state.usernotfound && !this.state.loading ?
              <PlayBack song ={this.state.songObj}/>
              : null
            }
            <Loader
              sizeUnit={"px"}
              css={{margin:'auto', justifySelf:'center'}}
              size={150}
              color={'#1ED760'}
              loading={this.state.loading}
            />
          </MuiThemeProvider>

      </div>
  );
  }
}

export default withStyles(styles)(UserPlayback);
