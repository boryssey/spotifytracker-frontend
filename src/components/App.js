import React, {Component} from 'react';
import Header from './Header'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { HashRouter as Router, Route, Link, Switch, Redirect } from "react-router-dom";
import grey from '@material-ui/core/colors/grey'
import UserPlayback from './UsersPlayback'
import Test from './test'
import NotFound from './404'
import Footer from './Footer'
import {Provider} from 'react-redux'
import Login from './Login'
import Me from './Me'
import PrivateRoute from './PrivateRoute'
import store from '../store'
import { CookiesProvider } from 'react-cookie';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#222326',
    },
    secondary: {
      light: '#FFFFFF',
      main: '#FFFFFF',
      contrastText: '#ffcc00',
    },
  },
  typography:{
    useNextVariants: true,
    fontFamily:"'Montserrat', sans-serif",
    h5:{
      fontWeight:'900',
    },
    h6:{
      fontWeight:'900'
    },
    subtitle1:{
      fontWeight:'900'
    }
  },
});

import '../styles/App.css';

class App extends Component {
  constructor(props) {
    super(props);
    document.title = 'Spotify Tracker'
  }
    render() {
        return (
          <Provider store={store}>
            <Router>
              <CookiesProvider>
                <MuiThemeProvider theme={theme}>
                  <div className='App' style={{background:'#222326', margin:0}}>
                    <Header/>
                    <Switch>
                      <Route exact path="/" component={Test}/>
                      <Route path="/users/:user" component={UserPlayback}/>
                      <Route path='/login' component={Login}/>
                      <PrivateRoute path='/me' component={Me}/>
                      <Route component={NotFound}/>
                    </Switch>
                    <Footer/>
                  </div>
                </MuiThemeProvider>
              </CookiesProvider>
            </Router>
          </Provider>
        );
    }
}

export default App;
