import React from 'react'
import {connect} from 'react-redux';
import FaceIcon from '@material-ui/icons/Face'
import {withStyles, makeStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import UserInfoApi from '../api/UserInfoApi'

const styles = theme => ({
  Container: {
    margin:'auto',
    backgroundColor:'#222326',
    display:'Grid',
    height:'100%',
  },
  GridContainer:{
    margin:'auto',
    maxWidth:'450px',
    marginTop:'75px'
  },
  UserIcon: {
    margin: 'auto',
    width:'350px',
    height:'auto',
    color:'white'
  },
  GridItem:{
    margin:'auto',
    flexBasis:'auto'
  },
  input:{
    display:'inline-block',
    border: '1px solid #e2e2e1',
    padding:'5px',
    borderRadius: 15,
    margin:'auto',
    color:'white',
    textAlign:'center',
    maxWidth:'160px'
  },
  changeButton:{
    border:'1px solid #1ED760',
    borderRadius: '50px',
    width:'150px',
    height:'50px',
    marginTop: '0.5vw',
    fontWeight:'900',
    color:'#1ED760'
  },
  profilePic:{
    margin: 'auto',
    width:'350px',
    height:'auto',
    borderRadius: '50%',
    marginBottom:'30px'
  }
})



class Login extends React.Component {
  constructor(props){
    document.title =  'Spotify Tracker - Me'
    super(props);
    this.state = {
      usernameInput: this.props.user.username,
      profilePic:''
    }
    this.handleInput = this.handleInput.bind(this)
    this.fetchData = this.fetchData.bind(this)
    UserInfoApi.getUserInfo(this.props.user.userId).then((response) => {
        console.log('wtf')
        console.log(response)
        if(response.status === 200){
          this.setState({
            profilePic: response.data.profilePic
          })
        } else{
          throw new Error(response.statusText);
        }
    }).catch((error)=>{
      console.log(error);
    })
  }

  componentDidMount(){
    console.log('wtf')
    this.fetchData()
    console.log('wtf')
  }
  handleInput(event){
    this.setState({ [event.target.id]: event.target.value });
  }
  fetchData(){
    console.log('wtf2')
    UserInfoApi.getUserInfo(this.props.user.userId).then((response) => {
        console.log('wtf')
        console.log(response)
        if(response.status === 200){
          this.setState({
            profilePic: response.data.profilePic
          })
        } else{
          throw new Error(response.statusText);
        }
    }).catch((error)=>{
      console.log('wtf error')
      console.log(error);
    })

  }
  render () {
    const link = 'http://localhost:8080/#/users/' + this.props.user.username;
    return (
      <div className={this.props.classes.Container}>
        <Grid container spacing={3} className={this.props.classes.GridContainer}>
          {this.state.profilePic === "" ? <FaceIcon className={this.props.classes.UserIcon}/> : <img src = {this.state.profilePic} className={this.props.classes.profilePic} placeholder='Profile Picture' />}
          <Grid item xs={12} className={this.props.classes.GridItem}>
            <Typography variant="subtitle1" color='primary' display="inline">Your link:   </Typography>
            <Link variant="subtitle1" color='secondary' display='inline' href={link}>{link}</Link>
          </Grid>
          <Grid item xs={12} className={this.props.classes.GridItem} style={{display:'flex'}}>
            <Typography variant="subtitle1" color='primary' display="inline" style={{margin:'5px 10px 5px 5px'}}>Username: </Typography>
            <TextField id="usernameInput" variant="filled" value={this.state.usernameInput} onChange={this.handleInput} InputProps={{disableUnderline:true, className:this.props.classes.input, inputProps:{style:{padding:'5px'}}, }}/>
          </Grid>
          <Grid item xs={12} className={this.props.classes.GridItem}>
          <Button className={this.props.classes.changeButton}>Change</Button>
        </Grid>
      </Grid>
    </div>
    )
  }

}
const mapStateToProps = state => ({
  authenticated: state.login.authenticated,
  user: state.login.user
})

export default connect(mapStateToProps, null)(withStyles(styles)(Login));
